import { Component, OnInit } from '@angular/core';
import { DatosService } from 'src/app/servicios/datos.service';

@Component({
  selector: 'app-comp1',
  templateUrl: './comp1.component.html',
  styleUrls: ['./comp1.component.css']
})
export class Comp1Component implements OnInit {

  mensaje1 = 'Hola Boton 1';

  constructor(private datosServicio:DatosService) { }

  ngOnInit(): void {
  }

  cambiarMensaje():void{
    this.datosServicio.mensaje=this.mensaje1;

    
  }
}

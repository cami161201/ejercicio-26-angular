import { Component, OnInit } from '@angular/core';
import { DatosService } from 'src/app/servicios/datos.service';
@Component({
  selector: 'app-comp2',
  templateUrl: './comp2.component.html',
  styleUrls: ['./comp2.component.css']
})
export class Comp2Component implements OnInit {
  mensaje2 = 'Hola Boton 2';

  constructor(private datosServicio:DatosService) { }

  ngOnInit(): void {
  }

  cambiarMensaje():void{
    this.datosServicio.mensaje=this.mensaje2;

    
  }
}

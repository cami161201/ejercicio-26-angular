import { Component, OnInit } from '@angular/core';
import { DatosService } from 'src/app/servicios/datos.service';
@Component({
  selector: 'app-comp3',
  templateUrl: './comp3.component.html',
  styleUrls: ['./comp3.component.css']
})
export class Comp3Component implements OnInit {

  mensaje3 = 'Hola Boton 3';

  constructor(private datosServicio:DatosService) { }

  ngOnInit(): void {
  }

  cambiarMensaje():void{
    this.datosServicio.mensaje=this.mensaje3;

    
  }
}
